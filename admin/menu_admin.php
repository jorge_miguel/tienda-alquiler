<nav class="navbar navbar-default">
  <div class="container-fluid">
    <div class="navbar-header">
      <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
        <span class="sr-only">Toggle navigation</span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
      </button>
      <a class="navbar-brand" href="listado_productos.php">ADMINISTRACIÓN</a>
    </div>
    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
      <ul class="nav navbar-nav">
        <li><a href="listado_productos.php"><i class="fa fa-cart-arrow-down" aria-hidden="true"></i> Listado de Productos</a></li>
        <li><a href="listado_categorias.php"><i class="fa fa-cart-arrow-down" aria-hidden="true"></i> Listado de Categorias</a></li>
        <li><a href="producto_agregar.php"><i class="fa fa-cart-plus" aria-hidden="true"></i> Agregar Productos Nuevos</a></li>
        <li><a href="categoria_agregar.php"><i class="fa fa-cart-plus" aria-hidden="true"></i> Agregar Categoria</a></li>
        <li><a href="listado_usuarios.php"><i class="fa fa-users" aria-hidden="true"></i> Listado de Usuarios</a></li>
        <li><a href="listado_compras.php"><i class="fa fa-users" aria-hidden="true"></i> Listado de Compras</a></li>
        <li><a href="logout.php"><i class="fa fa-sign-out" aria-hidden="true"></i> Salir</a></li>
      </ul>
    </div>
  </div>
</nav>