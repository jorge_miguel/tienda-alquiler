<?php 
error_reporting(E_ALL ^ E_NOTICE);
if(!isset($_SESSION))session_start();
if(!$_SESSION[admin_id]){
$_SESSION[volver]=$_SERVER['PHP_SELF']."?".$_SERVER['QUERY_STRING'];
header("Location: index.php");
}
require_once('../conexion.php'); ?>
<?php
	if($_POST[enviar] == "Modificar"){
		echo $q="UPDATE `compras` SET `cliente` = '$_POST[cliente]', `codigo` = '$_POST[codigo]', `nombre` = '$_POST[nombre]', `precio` = '$_POST[precio]', `cantidad` = '$_POST[cantidad]', `estado` = '$_POST[estado]', `fecha` = '$_POST[fecha]' WHERE `compras`.`id` = $_POST[id];";
		$resource=$conn->query($q);
		header("Location: listado_compras.php");
	}
?>
<?php
if($_GET[id]==0){
       header("Location: listado_compras.php"); 
        }
$query=" SELECT * FROM compras WHERE id='$_GET[id]'";
$resource = $conn->query($query); 
$total = $resource->num_rows;
$row = $resource->fetch_assoc();
?>

<!DOCTYPE html>
<html lang="es">
    <head> 
		<meta name="viewport" content="width=device-width, initial-scale=1">
        <meta charset="utf-8"> 
		<link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
		<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jquery.bootstrapvalidator/0.5.0/css/bootstrapValidator.min.css">
	
		<!-- Website Font style -->
	    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.6.1/css/font-awesome.min.css">
		<title>Modifica Compra</title>
		
		<style>
			#success_message{ 
				display: none;
			}
		</style>
		<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
		<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>
		<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-validator/0.4.5/js/bootstrapvalidator.min.js"></script>
	</head>
		<body>
         <?php 
            include("header.php"); 
            include("menu_admin.php"); 
        ?>
		    <div class="container">
			    <form class="well form-horizontal" method="post"  id="Modificar" name="Modificar"> 
					<fieldset>

					<!-- id de Formulario -->
					<legend><center><h2><b>Modifica Compra</b></h2></center></legend><br>

					<!-- id input-->

					<div class="form-group">
					  <label class="col-md-4 control-label">Cliente</label>  
					    <div class="col-md-4 inputGroupContainer">
					    <div class="input-group">
					        <span class="input-group-addon"><i class="glyphicon glyphicon-envelope"></i></span>
					  <input name="cliente" id="cliente" class="form-control"  type="cliente" value="<?php echo $row[cliente]?>">
					    </div>
					  </div>
					</div>
					
					<!-- Teléfono input-->
					       
					<div class="form-group">
					  <label class="col-md-4 control-label">Codigo</label>  
					    <div class="col-md-4 inputGroupContainer">
					    <div class="input-group">
					        <span class="input-group-addon"><i class="glyphicon glyphicon-earphone"></i></span>
					  <input name="codigo" id="codigo" class="form-control" type="text" value="<?php echo $row[codigo]?>" placeholder="+491">
					    </div>
					  </div>
					</div>

					<div class="form-group">
					  <label class="col-md-4 control-label">Nombre</label>  
					    <div class="col-md-4 inputGroupContainer">
					    <div class="input-group">
					        <span class="input-group-addon"><i class="glyphicon glyphicon-earphone"></i></span>
					  <input name="nombre" id="nombre" class="form-control" type="text" value="<?php echo $row[nombre]?>">
					    </div>
					  </div>
					</div>

                    <div class="form-group">
					  <label class="col-md-4 control-label">Precio</label>  
					    <div class="col-md-4 inputGroupContainer">
					    <div class="input-group">
					        <span class="input-group-addon"><i class="glyphicon glyphicon-earphone"></i></span>
					  <input name="precio" id="precio" class="form-control" type="text" value="<?php echo $row[precio]?>">
					    </div>
					  </div>
					</div>
					
                    <div class="form-group">
					  <label class="col-md-4 control-label">Cantidad</label>  
					    <div class="col-md-4 inputGroupContainer">
					    <div class="input-group">
					    <span class="input-group-addon"><i class="glyphicon glyphicon-earphone"></i></span>
					    <input name="cantidad" id="cantidad" class="form-control" type="text" value="<?php echo $row[cantidad]?>">
					    </div>
					  </div>
					</div>

					<!-- Select -->
					<div class="form-group"> 
						<label class="col-md-4 control-label">Estado</label>
							<div class="col-md-4 selectContainer">
								<div class="input-group">
									<span class="input-group-addon"><i class="glyphicon glyphicon-list"></i></span>
									<select name="estado" id="estado" class="form-control selectpicker" >
										<option value=" " >Seleccione el estado</option>
									 	<option value="pendiente">Pendiente</option>
									 	<option value="finalizado">Finalizado</option>
									</select>
							  </div>
							</div>
					</div>

					<!-- Dirección input-->
					       	      
					<div class="form-group">
					  <label class="col-md-4 control-label">Fecha</label>  
					    <div class="col-md-4 inputGroupContainer">
					    <div class="input-group">
					        <span class="input-group-addon"><i class="glyphicon glyphicon-home"></i></span>
					  <input name="fecha" id="fecha" class="form-control" type="text" value="<?php echo $row[fecha]?>">
					    </div>
					  </div>
					</div>
					
					
					<!-- Success message -->
					<div class="alert alert-success" role="alert" id="success_message">Success <i class="glyphicon glyphicon-thumbs-up"></i> Success!.</div>

					<!-- Button -->
					<div class="form-group">
					  <label class="col-md-4 control-label"></label>
					  <div class="col-md-4"><br>
					   <input type="submit" class="btn btn-success" name="enviar" value="Modificar" id="btn">
					  </div>
					</div>

					</fieldset>
					<input type="hidden" name="id" id="id" value="<?php echo $row[id]?>">
				</form>
				
			</div><!-- /.container -->
			 <?php 
                include("footer.php"); 
            ?>
		</body>
</html>